#include <unistd.h>
#include <sys/wait.h>
#include <iostream>

#include <libgen.h>
using namespace std;

// Clase Fork
class Fork {
  // Atributos privados
  private:
    // ID
    pid_t pid;
    int segundos;
    // Variable donde se encuntra el link
    char* url;

  // Atributos públicos
  public:
    // Constructor
    Fork (int seg, char* descargar) {
      // Variables
      url = descargar;
      segundos = seg;
      // Llama al procedimiento crear
      crear();
      // Llama al procedimiento ejecutarCodigo
      ejecutarCodigo();
    }

    // Métodos.
    void crear() {
      // crea el proceso hijo.
      pid = fork();
    }

    // Procedimiento que va a realizar la extraccion del audio y reproducirlo
    void ejecutarCodigo () {
      // valida la creación de proceso hijo.
      // Si es menos a 0 no se puede crear el proceso
      if (pid < 0) {
        cout << "No se pudo crear el proceso ...";
        cout << endl;

      // Si es igual a 0 se crea el proceso hijo y se extrae el audio
      } else if (pid == 0) {
        // Código del proceso hijo.
        cout << "Descargando video y extrayendo audio ...";
        cout << endl;

        /* Comandos utilizados para extraer el audio del video
        Se utiliza el -x para descargar el audio del video
        Se utiliza --keep-video para descargar el video
        Se utiliza --extract-audio para extraer el audio del video
        Se utiliza el --audio-format mp3 para descargar el audio con formato mp3
        Y se utiliza -o para cambiar el nombre del archivo y de esa forma obtener su ruta
        con %(title)s.%(ext)s se deja el audio con su nombre original y su respectiva extención
        */

        // Se extrae el audio del video
        execlp("youtube-dl", "-x", "--extract-audio", "--audio-format", "mp3", url, "-o", "%(title)s.%(ext)s", NULL);

        // Espera "segundos" para continuar (para pruebas).
        sleep(segundos);

      } else{
        // Código proceso padre.
        // Padre espera por el término del proceso hijo.
        wait (NULL);

        cout << "Reproduciendo canción ...";
        cout << endl;

        // Para reproducir el audio, se utiliza el programa sox
        // *.mp3 para reproducir todos los audios que terminen en mp3
        execlp("sox", "play", "*.mp3", NULL);
      }
    }
};

// Procedimiento principal
int main(int argc, char *argv[]){

  // Parametro de entrada
  char* url = argv[1];

  // Instanciación
  Fork fork(1, url);

  return 0;
}
