# Laboratorio 2 - Unidad II
**Módulo Sistemas Operativos y Redes - Creación de procesos con fork()**

Programa en C++ que permite dada una URL de un video en youtube, descargarlo y extraer el audio a formato mp3 (invocando el programa youtube-dl 1 ) y luego reproducirlo desde la terminal con algún reproductor.

Se utiliza fork para crear un proceso hijo que se encargue de descargar el video y extraer su audio para que
luego el proceso padre lo reproduzca.

El programa se debe ejecutar mediante un parámetro de entrada, el cual será el link de youtube que desee extraer el audio.


# Empezando
 
Se deben abrir los archivos por una terminar desde la localizacion de la carpeta correspondiente.
Se pidira ejecutar el programa mediante un parámetro de entrada (link de youtube), cuando se ejecuta se mostrará el mensake de "Descargando video y extrayendo audio ..." empezando a descargar el video y extrayendo el audio que el usuario quiere. Luego se mostará el mensaje de "Reproduciendo audio ...", donde se reproducira el audio por medio del programa Sox en la terminal.

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp

- Editor de texto

- Make:
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.

- Youtube-dl:
Se necesita instalar youtube-dl para poder descargar el video a traves de la plataforma de youtube. Se instala mediante los siguientes comandos:

Para instalarlo de inmediato para todos los usuarios de UNIX (Linux, OS X, etc.), y si tiene instalado curl, se debe usar: 

`sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl`

`sudo chmod a+rx /usr/local/bin/youtube-dl`

Si no tiene curl, alternativamente puede usar un wget reciente: 

`sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl`

`sudo chmod a+rx /usr/local/bin/youtube-dl`

Para extraer audio: Se necesitan ciertos paquetes para poder realizar la extracción del audio. El programa sugiere instalar ffmpeg / avconv y ffprobe / avprobe. Estós se instalan con el siguiente comando: 

`sudo apt-get install ffmpeg`

- Sox:
Se necesita instalar Sox, que es un programa que reproduce los mp3 desde la terminal de Ubuntu. Se instala mediante los siguientes comandos:

`sudo apt update && sudo apt install sox`

`sudo apt-get install libsox-fmt-all`

# Ejecutando las pruebas
Para ejecutar el programa se debe realizar los siguientes comandos en la terminal con la carpeta correspondiente:

`g++ main.cpp -o main`

`make`

`./main {URL}`

Se debe inicial el programa mediante un parámetro de entrado: Que puede ser cualquier link de youtube que desea reproducir su audio. 
Se mostrará que se está descargando y extrayendo audio, donde se indicará como se descarga el video y como finalmente se extrae su audio correpondiente quedando como formato final como .mp3. Luego se reproducira el audio y el programa finalizara cuando termine de reproducirse el audio. 

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación
- Librerias: iostream, unistd.h, sys / wait.h

# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl
